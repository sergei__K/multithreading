package CopyFile;

import java.io.*;

/**
 * Класс, который считывает из файила и записывает в файл
 */
public class Flows extends Thread {
    private String readr;
    private String writer;


    public Flows(String readr, String writer) {
        this.readr = readr;
        this.writer = writer;
    }
    public synchronized void run() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(readr));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(writer))) {
            String data;
            final long timeStart = System.currentTimeMillis();
            while ((data = bufferedReader.readLine()) != null) {
                bufferedWriter.write("\n"+data);
            }
            final long timeFinish = System.currentTimeMillis();
            System.out.println("Время = " + (timeFinish-timeStart));
        } catch (IOException e) {
            System.out.println("ERROR" + e);
        }
    }
}