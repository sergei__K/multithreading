package CopyFile;
/**
 * класс который создаёт и запускает потоки параллельно
 */
public class Main {
    public static void main(String[] args) {
        Flows flows1 = new Flows("src//CopyFile//data.txt","src//CopyFile//File1.txt");
        Flows flows2 = new Flows("src//CopyFile//data.txt","src//CopyFile//File2.txt");
        flows1.start();
        flows2.start();
    }
}