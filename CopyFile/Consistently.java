package CopyFile;

/**
 * класс который создаёт и запускает потоки последовательно
 */
public class Consistently {
    public static void main(String[] args) throws InterruptedException {
        Flows flows1 = new Flows("src//CopyFile//data.txt","src//CopyFile//File1.txt");
        Flows flows2 = new Flows("src//CopyFile//data.txt","src//CopyFile//File2.txt");
        flows1.start();
        flows1.join();
        flows2.start();
        flows2.join();
    }
}