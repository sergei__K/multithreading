package ReadingAndWriting;
/**
 * Класс создающий и запускающий потоки
 */
public class Main {
    public static void main(String[] args) {
        Flows flow1 = new Flows("src\\ReadingAndWriting\\file1.txt","src\\ReadingAndWriting\\result.txt");
        Flows flow2 = new Flows("src\\ReadingAndWriting\\file2.txt","src\\ReadingAndWriting\\result.txt");
        flow1.start();
        flow2.start();
    }
}
