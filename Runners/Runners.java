package Runners;

/**
 * Класс потоков демонстрирующий динамическое изменение приоритетов потоков
  */

public class Runners extends Thread {
    private String name;
    private int priority1;
    private int priority2;

    public Runners(String name, int priority1, int priority2) {
        this.name = name;
        this.priority1 = priority1;
        this.priority2 = priority2;
    }

    public void run() {
        for (int i = 0; i < 10000; i++) {
            setPriority(priority1);
            if (i==100){
                setPriority(priority2);
            }
            System.out.println(name+"  " +i+ " № круга");
        }
    }
}