package Runners;
/**
 * Класс создающий и запускающий потоки
 */
public class Main {
    public static void main(String[] args) {
        Runners runners1 =new Runners("бегун 1",1,10);
        Runners runners2 =new Runners("бегун 2",10,1);
        runners1.start();
        runners2.start();
    }
}