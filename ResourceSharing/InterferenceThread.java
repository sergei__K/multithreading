package ResourceSharing;

/**
 * Класс для представления потоков, инкрементирующих общую переменную {@code  private static int i}
 *
 * @author ITA
 */

public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private volatile static int i;

    public InterferenceThread(String name, InterferenceExample checker) {
        super(name);
        this.checker = checker;
    }

    public void run() {
        System.out.println(this.getName() + " запущен");
        while (!checker.stop()) {
            increment();
        }
        System.out.println(this.getName() + " завершен");
    }

    public synchronized static void increment() {
        ++i;
    }

    public int getI() {
        return i;
    }

}
