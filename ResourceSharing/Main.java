package ResourceSharing;

/**
 * Запуск примера, демонстрирующего потерю данных
 * @author ITA
 */

public class Main {
    public static void main(String[] args) throws InterruptedException {
        new InterferenceExample().example();
    }
}
