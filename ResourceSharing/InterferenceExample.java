package ResourceSharing;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Пример для сравнения результатов инкремента переменной counter
 * и переменной потоков i
 *
 * @author ITA
 */

public class InterferenceExample {
    private static final int HUNDRED = 100_000_000;
    private AtomicInteger counter = new AtomicInteger();

    public boolean stop() {
        return counter.incrementAndGet() > HUNDRED;
    }

    public void example() throws InterruptedException {
        InterferenceThread thread1 = new InterferenceThread("Поток 1", this);
        InterferenceThread thread2 = new InterferenceThread("Поток 2", this);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("Ожидаем: " + HUNDRED);
        System.out.println("Получили: " + thread2.getI());
    }
}
