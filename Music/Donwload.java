package Music;


import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Donwload extends Thread {
    private String strUrl;
    private String file;

    public Donwload(String strUrl, String file) {
        this.strUrl = strUrl;
        this.file = file;
    }

    public void run() {
        URL url = null;
        try {
            url = new URL(strUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream()); FileOutputStream stream = new FileOutputStream(file)) {
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}